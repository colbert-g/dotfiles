
These config files are quite special, since they are crafted for my specific setup and computer use :
- The keyboard layout is a [modified bépo](http://www.keyboard-layout-editor.com/#/gists/8ac014aee17fc77cd565650e2a0e9b2f) for a Preonic Keyboard, meaning that specific modifiers or key choices such as Ç or W might not be relevant outside this variation. 
It still need some improvements but I’ve been using this layout for about 1 year now and it’s quite robust (except the lack of the % key that is a major flaw I need to fix as soon as possible).
- Except for a bit of code and writing, I use mostly softwares dedicated to content creation (2D/3D) whiches rely heavily on mouse use. My window manager config needs to suit this by keeping all the essential shortcut on the left half of the keyboard (so my right hand doesn’t have to go back to the keyboard every time).

### Packages
##### Desktop environment :
- Window manager : i3
- Status bar : [polybar](https://polybar.github.io/)
- App launcher : [rofi](https://github.com/davatorium/rofi)
- File explorer : nemo
- Theming : [Kvantum (qt5)](https://github.com/tsujan/Kvantum/tree/master/Kvantum), [nord terminal](https://github.com/arcticicestudio/nord-gnome-terminal)

##### Content creation
- [Blender (Flatpak)](https://www.blender.org/)
	- [projectors](https://github.com/Ocupe/Projectors)
	- [sverchok](https://github.com/nortikin/sverchok/)
	- [import_3dm](https://github.com/jesterKing/import_3dm)
	- [nord dark (theme)](https://github.com/TehMerow/Blender_Nord_Dark_Theme)
- [Godot](https://godotengine.org/)
- [Splash](https://sat-metalab.gitlab.io/splash/)
- [Musescore (Flatpak)](https://musescore.org/fr)

##### Text editors
- [Neovim (Flatpak)](https://neovim.io/)
	- [vim-plug](https://github.com/junegunn/vim-plug)
	- [lightline](https://github.com/itchyny/lightline.vim)
	- [nordfox (theme)](https://github.com/EdenEast/nightfox.nvim)
- [Obsidian (Flatpak)](https://obsidian.md/)

##### Medias
- Music player : [Clementine](https://www.clementine-player.org/)
- PDF reader : [Zathura](https://pwmt.org/projects/zathura/)

##### Social
- [Discord (Flatpak)](https://discord.com/)
- [Signal (Flatpak)](https://www.signal.org/)

