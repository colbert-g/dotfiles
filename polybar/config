;==========================================================
; Polybar config file
;==========================================================

; color scheme based on nord
[colors]
background = #2e3440
background-alt = #4c566a
foreground = #eceff4
foreground-alt = #4c566a

low = #a3be8c
medium = #d08770
high = #bf616a

accent = #88c0d0
alert = #ebcb8b

[bar/main]
; ipc is used with pomobar
enable-ipc = true

;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 20
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

separator = |
line-size = 2

padding-left = 0
padding-right = 5
module-margin-left = 3
module-margin-right = 3

font-0 = "fixed:pixelsize=8;1"

modules-left = i3 
modules-center = clementine
modules-right = pomobar pulseaudio cpu temperature memory filesystem wlan eth date

tray-position = center
tray-padding = 2

scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

;------------------------------------------------------------
[module/i3]
type = internal/i3
format = <label-state> <label-mode>

; Split the workspace name on ':'
strip-wsnumbers = true
; Sort the workspaces by index instead of the default
index-sort = true
; No wrap when scroll through
wrapping-scroll = false

; Only show workspaces on the same output as the bar
pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = ${colors.background}
label-mode-background = ${colors.alert}

; focused = Active workspace on focused monitor
label-focused = %name%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.accent}
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %name%
label-unfocused-padding = 2

; visible = Active workspace on unfocused monitor
label-visible = %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %name%
label-urgent-underline = ${colors.alert}
label-urgent-foreground = ${colors.alert}
label-urgent-padding = 2

;------------------------------------------------------------
[module/clementine]
type = custom/script
exec = ~/.config/polybar/clementine_script/clementine_polybar.sh
label = %output%
format = <label>
interval = 1

format-foreground = ${colors.accent}
format-padding = 3

;------------------------------------------------------------
[module/pomobar]
type=custom/ipc
hook-0 = cat ~/.pomobaroutput
initial=1
; Left-click launch a pomo, right cancel one and middle reset total count
click-left = $HOME/.config/polybar/pomobar/pomobar.py --action=new &
click-middle= $HOME/.config/polybar/pomobar/pomobar.py --action=reset &
click-right = $HOME/.config/polybar/pomobar/pomobar.py --action=kill &

;------------------------------------------------------------
[module/pulseaudio]
type = internal/pulseaudio

; Interval for volume increase/decrease (in percent points)
interval = 5
use-ui-max = true
click-right = pavucontrol

format-volume = <label-volume> <bar-volume>
label-volume = "VOL %percentage%% "
label-volume-foreground = ${color.foreground}

label-muted = MUTE
label-muted-foreground = ${colors.foreground-alt}

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.low}
bar-volume-foreground-1 = ${colors.low}
bar-volume-foreground-2 = ${colors.low}
bar-volume-foreground-3 = ${colors.low}
bar-volume-foreground-4 = ${colors.medium}
bar-volume-foreground-5 = ${colors.medium}
bar-volume-foreground-6 = ${colors.high}
bar-volume-gradient = false
bar-volume-indicator = +
bar-volume-fill = ─
bar-volume-empty = ─
bar-volume-empty-foreground = ${colors.foreground-alt}

;------------------------------------------------------------
[module/cpu]
type = internal/cpu

; Seconds to sleep between updates
interval = 2

format = <label> <ramp-coreload>
label = "CPU "
ramp-coreload-spacing = 1
ramp-coreload-0 = ▁
ramp-coreload-0-font = 2
ramp-coreload-0-foreground = ${colors.low}
ramp-coreload-1 = ▂
ramp-coreload-1-font = 2
ramp-coreload-1-foreground = ${colors.low}
ramp-coreload-2 = ▃
ramp-coreload-2-font = 2
ramp-coreload-2-foreground = ${colors.low}
ramp-coreload-3 = ▄
ramp-coreload-3-font = 2
ramp-coreload-3-foreground = ${colors.low}
ramp-coreload-4 = ▅
ramp-coreload-4-font = 2
ramp-coreload-4-foreground = ${colors.medium}
ramp-coreload-5 = ▆
ramp-coreload-5-font = 2
ramp-coreload-5-foreground = ${colors.medium}
ramp-coreload-6 = ▇
ramp-coreload-6-font = 2
ramp-coreload-6-foreground = ${colors.high}
ramp-coreload-7 = █
ramp-coreload-7-font = 2
ramp-coreload-7-foreground = ${colors.high}

;------------------------------------------------------------
[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <label>
format-warn = <label-warn>

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.high}

;------------------------------------------------------------
[module/memory]
type = internal/memory
format = <label> <bar-used>
label = "RAM "

bar-used-width = 15
bar-used-foreground-0 = ${colors.low}
bar-used-foreground-1 = ${colors.low}
bar-used-foreground-2 = ${colors.medium}
bar-used-foreground-3 = ${colors.high}
bar-used-indicator = +
bar-used-indicator-foreground = ${colors.foreground}
bar-used-fill = ─
bar-used-empty = ─
bar-used-empty-foreground = ${colors.foreground-alt}


;------------------------------------------------------------
[module/wlan]
type = internal/network
interface = wlxd037455f073c
interval = 3.0

format-connected = <ramp-signal> <label-connected>
label-connected = " %essid%"
; future implementation - %downspeed% %upspeed%

format-disconnected = <label-disconnected>
label-disconnected = □ wifi
label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = ■
ramp-signal-0-foreground = ${colors.high}
ramp-signal-1 = ■
ramp-signal-1-foreground = ${colors.medium}
ramp-signal-2 = ■
ramp-signal-2-foreground = ${colors.alert}
ramp-signal-3 = ■
ramp-signal-3-foreground = ${colors.low}
ramp-signal-4 = ■
ramp-signal-4-foreground = ${colors.accent}
ramp-signal-foreground = ${colors.foreground}

;------------------------------------------------------------
[module/eth]
type = internal/network
interface = enp2s0
interval = 3.0

format-connected-prefix = "■ "
label-connected = %local_ip%

format-disconnected = <label-disconnected>
label-disconnected = □ ethernet
label-disconnected-foreground = ${colors.foreground-alt}

;------------------------------------------------------------
[module/date]
type = internal/date
interval = 5

date = " %d-%m "
date-alt = " %d-%m-%Y "

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}

label = "%date% •  %time%"

;------------------------------------------------------------
[settings]
screenchange-reload = true

[global/wm]
margin-top = 0
margin-bottom = 0
; vim:ft=dosini
