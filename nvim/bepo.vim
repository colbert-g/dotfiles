" Leader key on ç
let mapleader = "ç"

" [%] -> {é}
" ————————————————
" Percent more accessible + mark access
noremap é %
noremap É `

" [HJKL] -> {TSRN}
" ————————————————
" {tn} = « left / right »
noremap t h
noremap n l
" {sr} = « down / up »
noremap s j
noremap r k
" {SR} = « down / up half-page »
noremap S <C-D>
noremap R <C-U>
" {TN} = « beginning / end of line »
noremap T 0
noremap N $

" Manpage on $
noremap $ K
noremap <C-j> J

" [TSRN] -> {HJKL}
" ————————————————
" {J} = « jusqu'à »
noremap j t
noremap J T
" {H} = replace
noremap h r
noremap H R
" {K} = substitute
noremap k s
noremap K S
" {L} = « look at next / previous »
noremap l n
noremap L N

" Easy indent on « »
noremap « <<
noremap » >>

" Windows management
" ————————————————
nnoremap <Leader>t :wincmd h<CR>
nnoremap <Leader>s :wincmd j<CR>
nnoremap <Leader>r :wincmd k<CR>
nnoremap <Leader>n :wincmd l<CR>

" Tab management
" ————————————————
nnoremap <Leader>T gT
nnoremap <Leader>N gt
