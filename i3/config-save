# i3 config file (v4)

# Modifier assignation
set $mod Mod4
set $mod2 control

#-----------------------------
#----------LAUNCHERS----------
#-----------------------------

# start dmenu (a program launcher)
# bindsym $mod+i exec dmenu_run
# i3-dmenu-desktop only displays applications shipping a .desktop file
# « Application »
bindsym --release $mod+a exec "rofi -combi-modi window,drun -show combi -modi combi -disable-history"
bindsym --release $mod+$mod2+a exec "rofi -show drun"

# start terminal & explorer
bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+$mod2+Return exec "if hash nemo; then nemo; else nautilus; fi"

# Print screen 
bindsym --release $mod+equal exec --no-startup-id scrot -s 'capture_%Y-%m-%d_%H-%M-%S.png' -e 'mkdir -p ~/Images/captures && mv $f ~/Images/captures'
bindsym --release $mod+$mod2+equal exec --no-startup-id scrot 'capture_%Y-%m-%d_%H-%M-%S.png' -e 'mkdir -p ~/Images/captures && mv $f ~/Images/captures'

# kill focused window
bindsym $mod+k kill

#----------CONTROLS----------

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

# reload the configuration file
bindsym $mod+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+$mod2+c restart

#---------------------------
#----------WINDOWS----------
#---------------------------

# change focus
bindsym $mod+t focus left
bindsym $mod+s focus down
bindsym $mod+r focus up
bindsym $mod+n focus right

# move focused window
bindsym $mod+$mod2+t move left
bindsym $mod+$mod2+s move down
bindsym $mod+$mod2+r move up
bindsym $mod+$mod2+n move right

# split in horizontal/vertical orientation
bindsym $mod+h split h
bindsym $mod+v split v

# enter fullscreen mode for the focused container
# « Expend »
bindsym $mod+e fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+u layout stacking
bindsym $mod+eacute layout tabbed
bindsym $mod+p layout toggle split

#----------FLOATING----------

# toggle tiling / floating - change focus
bindsym $mod+f floating toggle
bindsym $mod+space focus mode_toggle

# focus the parent/child container
#bindsym $mod+a focus parent
bindsym $mod+b focus child

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym t resize shrink width 10 px or 10 ppt
        bindsym s resize grow height 10 px or 10 ppt
        bindsym r resize shrink height 10 px or 10 ppt
        bindsym n resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsy Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+o mode "default"
}
bindsym $mod+o mode "resize"

#------------------------------
#----------WORKSPACES----------
#------------------------------

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1:Code"
set $ws2 "2:Net"
set $ws3 "3"
set $ws4 "4:Media"
set $ws5 "5:Social"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9:Mail"
set $ws10 "10:VM"

# switch to workspace
bindsym $mod+quotedbl workspace number $ws1
bindsym $mod+guillemotleft workspace number $ws2
bindsym $mod+guillemotright workspace number $ws3
bindsym $mod+parenleft workspace number $ws4
bindsym $mod+parenright workspace number $ws5
bindsym $mod+at workspace number $ws6
bindsym $mod+plus workspace number $ws7
bindsym $mod+minus workspace number $ws8
bindsym $mod+slash workspace number $ws9
bindsym $mod+asterisk workspace number $ws10

# move focused container to workspace
bindsym $mod+$mod2+quotedbl move container to workspace number $ws1
bindsym $mod+$mod2+guillemotleft move container to workspace number $ws2
bindsym $mod+$mod2+guillemotright move container to workspace number $ws3
bindsym $mod+$mod2+parenleft move container to workspace number $ws4
bindsym $mod+$mod2+parenright move container to workspace number $ws5
bindsym $mod+$mod2+at move container to workspace number $ws6
bindsym $mod+$mod2+plus move container to workspace number $ws7
bindsym $mod+$mod2+minus move container to workspace number $ws8
bindsym $mod+$mod2+slash move container to workspace number $ws9
bindsym $mod+$mod2+asterisk move container to workspace number $ws10

# scratchpad (minimize and restore)
bindsym $mod+$mod2+dollar move scratchpad
bindsym $mod+dollar scratchpad show

#--------ASSIGNATIONS--------

for_window [window_role="pop-up"] floating enable
for_window [window_role="Preference"] floating enable
for_window [window_type="menu"] floating enable
for_window [class="Splash.*"] floating enable
#for_window [class="Blender.*"] floating enable
for_window [title="Blender Preferences"] floating enable, move position center
for_window [class="Pavucontrol"] floating enable
for_window [class="Zathura"] floating enable resize set 900 1000, move position center
for_window [class="Rhinoceros.*"] floating enable
assign [class="Firefox"] $ws2
assign [class="Clementine"] $ws4

# social - tabbed
for_window [class="Slack"] layout tabbed
assign [class="Slack"] $ws5
for_window [class="discord"] layout tabbed
assign [class="discord" instance="discord"] $ws5
assign [class="Zulip"] $ws5
assign [class="Signal"] $ws5

# for_window [class="Thunderbird.*"] floating enable, move position center
assign [class="Thunderbird"] $ws9

for_window [class="Tape"] move scratchpad

#---------------------------
#----------DISPLAY----------
#---------------------------

# Font for window titles. Also used by the bar unless a different font specified
font pango:Open Sans 8

# Color scheme
# class                 border  backgr. text    indicator child_border
client.focused          #88c0d0 #88c0d0 #eceff4 #5e81ac
client.focused_inactive #81a1c1 #81a1c1 #d8dee9 #81a1c1
client.unfocused        #5e81ac #5e81ac #d8dee9 #5e81ac
client.urgent           #ebcb8b #ebcb8b #eceff4 #ebcb8b

for_window [class=".*"] border pixel 3
new_window normal 1
new_float normal
# hide_edge_borders both
focus_follows_mouse no
mouse_warping none
workspace_auto_back_and_forth yes

# Polybar
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# Background
set $wallpaper ~/Images/Backgrounds/aperture.jpg
exec_always "feh --bg-fill $wallpaper"

#------------------------------
#----------LOCK+POWER----------
#------------------------------

# Autolock
exec --no-startup-id "xscreensaver"

# Lock, Logout, Suspend, HIbernate, Reboot and Shutdown
set $locker xscreensaver-command -lock
set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (ctrl+r) reboot, (ctrl+s) shutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id $locker, mode "default"
    bindsym e exec --no-startup-id i3-msg exit, mode "default"
    bindsym s exec --no-startup-id $locker && systemctl suspend, mode "default"
    bindsym h exec --no-startup-id $locker && systemctl hibernate, mode "default"
    bindsym $mod2+r exec --no-startup-id systemctl reboot && virsh destroy RDPWindows, mode "default"
    bindsym $mod2+s exec --no-startup-id systemctl poweroff -i && virsh destroy RDPWindows, mode "default"  
    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Escape mode "$mode_system"

#-----------------------------
#----------AUTOSTART----------
#-----------------------------

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
exec --no-startup-id nm-applet

# exec --no-startup-id slack
# exec --no-startup-id discord
# exec --no-startup-id signal-desktop

# mails
# exec --no-startup-id protonmail-bridge --no-window
# exec --no-startup-id thunderbird

# tape board
exec --no-startup-id /home/corentin/.app/aeriform_tape_1.3.6_linux_r2021032301/aeriform_tape_1.3.6_linux.AppImage

#----------------------------------------

# exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
# exec --no-startup-id /usr/libexec/gnome-settings-daemon
# exec --no-startup-id /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
# font pango:DejaVu Sans Mono 8
# font pango:Terminus 11px

# The combination of xss-lock, nm-applet and pactl is a popular choice, so
# they are included here as an example. Modify as you see fit.

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
#exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

# exit i3 (logs you out of your X session)
#bindsym $mod+$mod2+Escape exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you #really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
#bar {
#	position top
#	strip_workspace_numbers yes
#        status_command i3status
        
#        colors {
#	    separator #586e75
#	    background #002b36
#	    statusline #aea79f
#	    focused_workspace #586e75 #586e75 #ffffff
#	    active_workspace #073642 #073642 #ffffff
#	    inactive_workspace #002b36 #002b36 #aea79f
#	    urgent_workspace #0083A4 #0083A4 #ffffff
#	  }
  	
#}


